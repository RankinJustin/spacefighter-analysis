
#include "EnemyShip.h"


EnemyShip::EnemyShip()
{
	SetMaxHitPoints(1);
	SetCollisionRadius(20);
}


void EnemyShip::Update(const GameTime *pGameTime)
{
	if (m_delaySeconds > 0) // If there is a delay enter loop
	{
		m_delaySeconds -= pGameTime->GetTimeElapsed(); // subtract the delay by the time elapsed in game

		if (m_delaySeconds <= 0)
		{
			// Once the delay is over, activate enemy ship
			GameObject::Activate();
		}
	}

	if (IsActive()) // If the ship is active enter loop
	{
		m_activationSeconds += pGameTime->GetTimeElapsed(); // add the activation time to the time elapsed in game
		if (m_activationSeconds > 2 && !IsOnScreen()) Deactivate(); 
		// If the ship has already been activated, and it is off screen, deactivate it
	}

	// Update the ship based on the game time, moving
	Ship::Update(pGameTime);
}


void EnemyShip::Initialize(const Vector2 position, const double delaySeconds)
{
	SetPosition(position);
	m_delaySeconds = delaySeconds;

	Ship::Initialize();
}


void EnemyShip::Hit(const float damage)
{
	Ship::Hit(damage);
}