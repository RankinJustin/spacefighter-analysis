

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{
	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	// Sets the count for the number of ships in the level
	const int COUNT = 21;

	// Sets the positions the enemies spawn at in the level
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};
	
	// Sets a delay for when the enemies will spawn after the initial delay
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 1.0; // start delay, changed the delay
	Vector2 position;

	for (int i = 0; i < COUNT; i++) // Loop to spawn the enemies
	{
		delay += delays[i]; // Assigns array element to variable
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y); // Sets the position of the enemies
		//based on the width of the screen, centers texture when getting position

		BioEnemyShip *pEnemy = new BioEnemyShip(); // Creates new enemy 
		pEnemy->SetTexture(pTexture); // Gives enemy texture
		pEnemy->SetCurrentLevel(this); // Sets the enemy to the current level
		pEnemy->Initialize(position, (float)delay); // sets the enemies position when spawing, sets delay for enemy
		AddGameObject(pEnemy); //Adds the enemy to the level
	}

	// Loads the level onto the screen
	Level::LoadContent(pResourceManager);
}

